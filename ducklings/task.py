import asyncio
import time
from dataclasses import dataclass, field
from typing import Callable, Optional, Iterator, List
from enum import Enum
from colorama import Fore, Style

CHECK_MARK = "\u2713"
X_MARK = "\u2717"


class Spinner:
    frames: List[str] = ["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠧", "⠇", "⠏"]
    interval: float = 0.080
    __frame: int = 0
    __last_frame: float = 0

    def __iter__(self) -> Iterator[str]:
        return self

    def __next__(self) -> str:
        t = time.time()
        if (self.__last_frame + self.interval) < t:
            self.__frame = (self.__frame + 1) % len(self.frames)
            self.__last_frame = t
        return self.frames[self.__frame]


class Status(Enum):
    FINISHED = 0
    COOLDOWN = 1
    RUNNING = 2


@dataclass(order=True)
class Task:
    name: str
    worker_func: Callable
    cooldown: float = 0  # seconds
    position: int = field(default=0, compare=True, hash=False, repr=False)
    __spinner: Spinner = field(default_factory=Spinner, hash=False, repr=False)
    __created: float = field(default_factory=time.time, hash=False, repr=False)
    __finished: int = field(default=-1, hash=False, repr=False)
    __state: Status = Status.RUNNING
    __error: Optional[Exception] = field(default=None, hash=False, repr=False)
    __result: str = field(default_factory=str, hash=False, repr=False)

    def __post_init__(self):
        coro = self.worker_func(self.name)
        loop = asyncio.get_event_loop()
        self.__task = loop.create_task(coro)

    @property
    def status(self):
        if self.__state is Status.RUNNING:
            if self.__task.done():
                self.__finished = int(time.time())

                try:
                    self.__result = self.__task.result()
                except Exception as e:
                    self.__error = e

                if self.__task.cancelled():
                    self.__error = asyncio.CancelledError("Task cancelled")

                # Skip the cooldown for errors
                if self.__error:
                    self.__state = Status.FINISHED
                else:
                    self.__state = Status.COOLDOWN

        if self.__state is Status.COOLDOWN:
            if self.__finished + self.cooldown < time.time():
                self.__state = Status.FINISHED

        return self.__state

    def __str__(self):
        status = self.status
        if status is Status.RUNNING:
            return f"{Fore.CYAN}{next(self.__spinner)}{Style.RESET_ALL} {self.name}"
        elif status is Status.COOLDOWN:
            if self.__result:
                return f"{Fore.GREEN}{next(self.__spinner)} {self.name}{Style.RESET_ALL}: {self.__result}"
            return f"{Fore.GREEN}{next(self.__spinner)} {self.name}{Style.RESET_ALL}"
        elif status is Status.FINISHED:
            if self.__error:
                return f"{Fore.RED}{X_MARK} {self.name}{Style.RESET_ALL}: {repr(self.__error)}"

            if self.__result:
                return f"{Fore.GREEN}{CHECK_MARK} {self.name}{Style.RESET_ALL}: {self.__result}"

            return f"{Fore.GREEN}{CHECK_MARK} {self.name}{Style.RESET_ALL}"

        raise NotImplementedError("Unreachable code encountered!")
