import asyncio
import math
import sys
from sys import stdout
from dataclasses import dataclass
from typing import List, Iterable, Callable, Optional, Union
from functools import partial
from blessings import Terminal
import colorama
from loguru import logger
from .task import Task, Status


@dataclass
class Raft:
    """You will be dealing with the results of each job you enter into the queue."""

    op_func: Callable
    concurrent: int = 1
    cooldown: float = 1  # seconds
    tick_rate: int = 64
    __jobs_finished: bool = False

    def __call__(self, jobs: Iterable):
        work_queue_size = math.sqrt(self.concurrent)
        work_queue_size = math.ceil(work_queue_size)
        work_queue: asyncio.Queue = asyncio.Queue(maxsize=work_queue_size)

        loop = asyncio.get_event_loop()
        consumer = loop.create_task(self.__consumer(jobs, work_queue))
        executor = loop.create_task(self.__executor(work_queue))
        tasks = asyncio.gather(consumer, executor)
        loop.run_until_complete(tasks)

    # Ideally, I would like to be able to let everyone set up their generator
    # and hand them a results queue to do with as they please, buuut. y'know.
    # def __enter__(self):
    #    colorama.init()
    #    self.__term = Terminal()
    #    queue = asyncio.Queue()
    #    return self.status_screen(self.work_queue)
    #
    # def __exit__(self):
    #    colorama.deinit()

    # OPTIONS:

    # I can reorganize the jobs structure (Task)

    # I can split the job_queue into another thread/async to ensure
    # that stopiteration is handled gracefully, and set a flag to
    # let the work_queue know that we're done here. That queue consumer
    # can do the responsible thing and only buffer a few things at a time, too.

    async def __consumer(self, jobs, work_queue):
        jobs = iter(jobs)
        while True:
            if work_queue.full():
                await asyncio.sleep(1 / self.tick_rate)
                continue

            # If this raises QueueFull, something else is wrong.
            try:
                job = next(jobs)
                work_queue.put_nowait(job)
            except asyncio.QueueFull as e:
                logger.warning(
                    "An error has occurred inserting jobs into our job queue!"
                )
                raise e
            except StopIteration:
                break

        self.__jobs_finished = True

    async def __executor(self, work_queue: asyncio.Queue):
        workers: List[Task] = []
        term = Terminal()
        colorama.init()

        while True:
            finished_work = await self.__finished_work(term, workers)
            workers = [worker for worker in workers if worker not in finished_work]
            await self.__update_display_list(term, workers)
            if not work_queue.empty():
                workers = await self.__new_workers(work_queue, workers)
            stdout.flush()

            # Exit loop if no work is left
            if all((self.__jobs_finished, work_queue.empty(), len(workers) == 0)):
                break
            else:
                await asyncio.sleep(1 / self.tick_rate)

        colorama.deinit()

    async def __finished_work(self, term: Terminal, workers: List[Task]) -> List[Task]:
        finished_work = [work for work in workers if work.status is Status.FINISHED]
        await self.__update_display_list(term, finished_work)
        return finished_work

    @staticmethod
    async def __update_display_list(term: Terminal, workers: List[Task]):
        with term.hidden_cursor():
            for work in workers:
                with term.location(x=0):
                    print(term.move_up * work.position, end="")
                    print(work, end="")

    async def __new_workers(self, work_queue: asyncio.Queue, workers: List[Task]):
        """Add new workers as available"""
        workers_available = self.concurrent - len(workers)
        workers_needed = min(workers_available, work_queue.qsize())
        for pos, _ in enumerate(range(workers_needed), start=-(workers_needed - 1)):
            name = work_queue.get_nowait()
            work = Task(name, self.op_func, self.cooldown)
            work.position = pos
            workers.append(work)
            print(work)

        for work in workers:
            work.position += workers_needed

        return sorted(workers, reverse=True)


def raft(
    func: Optional[Callable] = None, *, cooldown: int = 0, concurrent: int = 1, tick_rate: int = 64
) -> Union[Raft, Callable]:
    if func is None:
        return partial(
            raft, cooldown=cooldown, concurrent=concurrent, tick_rate=tick_rate
        )
    if not asyncio.iscoroutinefunction(func):
        raise Exception("Async functions are required for use with Raft!")
    return Raft(func, cooldown=cooldown, concurrent=concurrent, tick_rate=tick_rate)
