import asyncio
import random
from random import randint
from ducklings import raft


@raft(cooldown=2, concurrent=10, tick_rate=200)
async def random_failures(item):
    wait = int(random.random()*100) % 2 # DEBUG
    await asyncio.sleep(wait)
    if randint(0, 1):
        raise Exception("Random failure!", item)
    if randint(0, 1):
        return "Example data"
    return None

random_failures(range(0, 100_000))
