from ducklings import raft
from random import randint


@raft(concurrent=4)
async def random_failures(item):
    if randint(0, 1):
        raise Exception("Random failure!", item)
    if randint(0, 1):
        return "Example data"
    return None

random_failures(range(0, 10))
