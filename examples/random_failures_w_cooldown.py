from ducklings import raft
from random import randint


@raft(cooldown=2, concurrent=4, tick_rate=20)
async def random_failures(item):
    if randint(0, 1):
        raise Exception("Random failure!", item)
    if randint(0, 1):
        return "Example data"
    return None

random_failures(range(0, 10))
