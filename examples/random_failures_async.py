from ducklings import Raft
from random import randint


@Raft
async def random_failures_async(_):
    if randint(0, 1):
        raise Exception("Random failure!")
    if randint(0, 1):
        return "Example data"
    return


random_failures_async(range(0, 20))
